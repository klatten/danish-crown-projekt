package storage;

import java.util.ArrayList;
import java.util.List;

import model.Chauffør;
import model.Lastbil;
import model.Ordre;
import model.Rampe;
import model.Trailer;

public class Storage {
	private static List<Lastbil> lastbiler = new ArrayList<Lastbil>();
	private static List<Chauffør> chauffører = new ArrayList<Chauffør>();
	private static List<Ordre> ordrer = new ArrayList<Ordre>();
	private static List<Trailer> trailere = new ArrayList<Trailer>();
	private static List<Rampe> ramper = new ArrayList<Rampe>();

	/**
	 *
	 * Retunerer lister over de gemte objecter
	 */
	public static List<Lastbil> getAllLastbiler() {
		return new ArrayList<Lastbil>(lastbiler);
	}

	public static List<Chauffør> getAllChauffører() {
		return new ArrayList<Chauffør>(chauffører);
	}

	public static List<Ordre> getAllOrdrer() {
		return new ArrayList<Ordre>(ordrer);
	}

	public static List<Trailer> getAllTrailerer() {
		return new ArrayList<Trailer>(trailere);
	}

	public static List<Rampe> getAllRamper() {
		return new ArrayList<Rampe>(ramper);
	}

	public static void gemRamper(Rampe rampe) {
		if (!ramper.contains(rampe)) {
			ramper.add(rampe);
		}
	}

	public static void gemLastbil(Lastbil lastbil) {
		if (!lastbiler.contains(lastbil)) {
			lastbiler.add(lastbil);
		}
	}

	/**
	 *
	 * Gemmer de forskellige objekter
	 */

	public static void gemChauffør(Chauffør chauffør) {
		if (!chauffører.contains(chauffør)) {
			chauffører.add(chauffør);
		}
	}

	public static void gemOrdre(Ordre ordre) {
		if (!ordrer.contains(ordre)) {
			ordrer.add(ordre);
		}
	}

	public static void gemTrailer(Trailer trailer) {
		if (!trailere.contains(trailer)) {
			trailere.add(trailer);
		}
	}

}
