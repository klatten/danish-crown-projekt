package guiFX;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import service.Service;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Danish Crown");
		BorderPane pane = new BorderPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		primaryStage.setScene(scene);
		primaryStage.setHeight(500);
		primaryStage.setWidth(800);
		primaryStage.show();
	}

	@Override
	public void init() {
		Service.createSomeObjects();
	}

	private void initContent(BorderPane pane) {
		TabPane tabPane = new TabPane();
		this.initTabPane(tabPane);
		pane.setCenter(tabPane);
	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		Tab tabMeld = new Tab("Meld");
		Tab tabVis = new Tab("Vis rampeoversigt");
		Tab tabStatistik = new Tab("Vis statistik");
		Tab tidTab = new Tab("Tidssimulator");

		tabMeld.setContent(new AnkomstPane());
		VisRampeOversigtPane visRampeOversigtPane = new VisRampeOversigtPane();
		tabVis.setContent(visRampeOversigtPane);
		tabStatistik.setContent(new StatistikPane());
		tidTab.setContent(new TidPane());

		tabPane.getTabs().add(tabMeld);
		tabPane.getTabs().add(tabVis);
		tabPane.getTabs().add(tabStatistik);
		tabPane.getTabs().add(tidTab);

	}

	// Button Actions

	// public void meldAnkomstAction() {
	// AnkomstDialog dia = new AnkomstDialog();
	// }

}
