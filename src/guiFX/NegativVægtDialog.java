package guiFX;

import java.time.LocalTime;
import java.util.ArrayList;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Ankomst;
import model.Delordre;
import model.Ordre;
import model.Type;

public class NegativVægtDialog extends Stage {

	public NegativVægtDialog(String title) {
		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);
		setResizable(false);

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	// -------------------------------------------------------------------------

	private ListView<Ankomst> lvwAnkomster = new ListView<>();
	private ArrayList<Ankomst> ankomster = new ArrayList<>();
	private TextField txtslutvægt;
	private Label lblInfo;
	private Label lblForventetVægt;
	private Label lblMåltSlutVægt;
	private Label lblMargen;
	private Label lblnyAfgangstid;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		lblInfo = new Label("Vægt passer ikke");
		pane.add(lblInfo, 0, 0);
		int tid = 0;
		Ordre ordrer = new Ordre(Type.PALLE, 0, 0, 0);
		for (Delordre delordre : AfgangDialog.givAnkomst().getLastbil()
				.getDelordrer()) {
			ordrer = delordre.getOrdre();
			tid += delordre.getLæssetid();

		}
		double vægt = 0.0;
		for (Delordre delordre : AfgangDialog.givAnkomst().getLastbil()
				.getDelordrer()) {
			ordrer = delordre.getOrdre();
			vægt += delordre.getVægt();
		}

		lblForventetVægt = new Label("Forventet vægt:" + " "
				+ (AfgangDialog.givAnkomst().getStartVægt() + vægt));
		pane.add(lblForventetVægt, 0, 1);
		lblMåltSlutVægt = new Label("Målt slut vægt: " + " "
				+ AfgangDialog.givText());
		pane.add(lblMåltSlutVægt, 0, 2);

		lblMargen = new Label("Accepteret fejlmargen: " + " "
				+ ordrer.getMargenVedVejning() + "%");
		pane.add(lblMargen, 0, 3);

		lblnyAfgangstid = new Label("Ny Afgangstid: " + " "
				+ LocalTime.now().plusMinutes(tid));

		Button btnCancel = new Button("Ok");
		pane.add(btnCancel, 0, 12);
		GridPane.setHalignment(btnCancel, HPos.RIGHT);
		btnCancel.setOnAction(event -> cancelAction());
	}

	private void cancelAction() {
		hide();
		close();

	}

}
