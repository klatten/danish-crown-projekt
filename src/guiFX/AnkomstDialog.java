package guiFX;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Ankomst;
import model.Chauffør;
import model.Delordre;
import model.Lastbil;
import model.Ordre;
import model.Status;
import model.Trailer;
import model.Type;
import service.Service;
import storage.Storage;

public class AnkomstDialog extends Stage {

	private Trailer trailer;

	public AnkomstDialog(String title, Chauffør chauffør) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.trailer = trailer;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	public AnkomstDialog(String title) {
		this(title, null);
	}

	// -------------------------------------------------------------------------

	private Label lblError;

	private static ListView<Chauffør> lvwChaufføre = new ListView<>();
	private static ListView<Trailer> lvwTrailers = new ListView<>();
	private ListView<Delordre> lvwDelordrer = new ListView<>();
	private ListView<Ordre> lvwOrdrer = new ListView<>();
	private Lastbil lastbil;
	public static Ankomst anko;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblChaufførNavn = new Label("Chauffører");
		pane.add(lblChaufførNavn, 0, 0);

		pane.add(AnkomstDialog.lvwChaufføre, 0, 1, 1, 1);
		AnkomstDialog.lvwChaufføre.setPrefHeight(125);
		AnkomstDialog.lvwChaufføre.setPrefWidth(250);
		AnkomstDialog.lvwChaufføre.getItems()
		.setAll(Storage.getAllChauffører());

		Button btnChaufføre = new Button("Create Chaufføre");
		pane.add(btnChaufføre, 0, 2);
		GridPane.setHalignment(btnChaufføre, HPos.LEFT);
		btnChaufføre.setOnAction(event -> this.createChauffør());

		Label lblTrailer = new Label("Trailers");
		pane.add(lblTrailer, 0, 3);

		pane.add(AnkomstDialog.lvwTrailers, 0, 4, 1, 1);
		AnkomstDialog.lvwTrailers.setPrefHeight(125);
		AnkomstDialog.lvwTrailers.setPrefWidth(250);
		AnkomstDialog.lvwTrailers.getItems().setAll(Storage.getAllTrailerer());

		Button btnTrailer = new Button("Create Trailer");
		pane.add(btnTrailer, 0, 5);
		GridPane.setHalignment(btnTrailer, HPos.LEFT);
		btnTrailer.setOnAction(event -> this.createTrailer());

		Label lblOrdre = new Label("Ordre");
		pane.add(lblOrdre, 0, 6);

		pane.add(lvwOrdrer, 0, 7, 1, 1);
		lvwOrdrer.setPrefHeight(125);
		lvwOrdrer.setPrefWidth(250);

		Label lblDelordre = new Label("Delordre");
		pane.add(lblDelordre, 0, 8);

		pane.add(lvwDelordrer, 0, 9, 1, 1);
		lvwDelordrer.setPrefHeight(125);
		lvwDelordrer.setPrefWidth(250);
		lvwDelordrer.getSelectionModel().setSelectionMode(
				SelectionMode.MULTIPLE);
		lvwDelordrer.getSelectionModel().getSelectedItems();

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 12);
		GridPane.setHalignment(btnCancel, HPos.RIGHT);
		btnCancel.setOnAction(event -> this.cancelAction());

		Button btnCreate = new Button("Create Ankomst");
		pane.add(btnCreate, 0, 12);
		GridPane.setHalignment(btnCreate, HPos.LEFT);
		btnCreate.setOnAction(event -> this.createAnkomst());

		lblError = new Label();
		pane.add(lblError, 0, 11);
		lblError.setStyle("-fx-text-fill: red");

		ChangeListener<Ordre> listenere = (ov, oldOrdre, newOrdre) -> this
				.selectedOrdreChanged();
		lvwOrdrer.getSelectionModel().selectedItemProperty()
		.addListener(listenere);

		ChangeListener<Trailer> listeneres = (ov, oldTrailer, newTrailer) -> this
				.selectedTrailerChanged();
		AnkomstDialog.lvwTrailers.getSelectionModel().selectedItemProperty()
		.addListener(listeneres);

	}

	// -------------------------------------------------------------------------

	private void cancelAction() {
		this.hide();
		this.close();
	}

	private void createChauffør() {
		ChaufførDialog dia = new ChaufførDialog("Create Chauffør");
		dia.showAndWait();

		AnkomstDialog.lvwChaufføre.getItems()
		.setAll(Storage.getAllChauffører());
		int index = AnkomstDialog.lvwChaufføre.getItems().size() - 1;
		AnkomstDialog.lvwChaufføre.getSelectionModel().select(index);
	}

	private void createTrailer() {
		TrailerDialog dia = new TrailerDialog("Create Trailer");
		dia.showAndWait();

		AnkomstDialog.lvwTrailers.getItems().setAll(Storage.getAllTrailerer());
		int index = AnkomstDialog.lvwTrailers.getItems().size() - 1;
		AnkomstDialog.lvwTrailers.getSelectionModel().select(index);
	}

	private static Lastbil createLastbil() {
		Chauffør chauffør = AnkomstDialog.lvwChaufføre.getSelectionModel()
				.getSelectedItem();
		Trailer trailer = AnkomstDialog.lvwTrailers.getSelectionModel()
				.getSelectedItem();
		Lastbil lastbil = Service.createLastbil(Storage.getAllLastbiler()
				.size() + 1, chauffør, trailer);
		return lastbil;

	}

	private void createAnkomst() {

		lblError.setText("");
		if (AnkomstDialog.lvwChaufføre.getSelectionModel().getSelectedItem() == null) {
			lblError.setText("Du skal vælge chauffør.");
		} else if (AnkomstDialog.lvwTrailers.getSelectionModel()
				.getSelectedItem() == null) {
			lblError.setText("Du skal vælge trailer.");
		} else if (lvwOrdrer.getSelectionModel().getSelectedItem() == null) {
			lblError.setText("Du skal vælge ordrer.");
		} else if (lvwDelordrer.getSelectionModel().getSelectedItem() == null) {
			lblError.setText("Du skal vælge delordrer.");

		} else {
			Ordre ordrer = lvwOrdrer.getSelectionModel().getSelectedItem();
			List<Delordre> delordre = lvwDelordrer.getSelectionModel()
					.getSelectedItems();

			Ankomst ankomst = Service.createAnkomst(LocalTime.now(),
					AnkomstDialog.createLastbil());
			for (Delordre delordrer : delordre) {
				ankomst.getLastbil().addDelordre(delordrer);
			}
			Service.acceptRampeValg(ankomst);

			AnkomstDialog.anko = ankomst;
			ankomst.getLastbil().setStatus(Status.INAKTIV);

			InformationDialog dia = new InformationDialog("Information");
			dia.showAndWait();
			this.hide();
		}
	}

	public static Ankomst givAnkomst() {
		return AnkomstDialog.anko;
	}

	private void selectedOrdreChanged() {
		if (lvwOrdrer.getSelectionModel().getSelectedItem() != null) {
			lvwDelordrer.getItems().setAll(
					lvwOrdrer.getSelectionModel().getSelectedItem()
					.getDelordrer());

		}

	}

	private void selectedTrailerChanged() {
		ArrayList<Ordre> ord = new ArrayList<>();
		if (AnkomstDialog.lvwTrailers.getSelectionModel().getSelectedItem() != null) {
			Type type1 = AnkomstDialog.lvwTrailers.getSelectionModel()
					.getSelectedItem().getType();
			for (Ordre ordre : Storage.getAllOrdrer()) {
				if (ordre.getType() == type1) {
					for (Delordre delordre : ordre.getDelordrer()) {
						if (delordre.getVægt() < AnkomstDialog.lvwTrailers
								.getSelectionModel().getSelectedItem()
								.getKapacitet()) {
							if (!ord.contains(delordre.getOrdre())) {
								ord.add(delordre.getOrdre());
							}

						}
					}

				}

			}
		}
		lvwOrdrer.getItems().setAll(ord);

	}

}
