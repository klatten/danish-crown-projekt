package guiFX;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Trailer;
import model.Type;
import service.Service;

public class TrailerDialog extends Stage {

	private Trailer trailer;

	public TrailerDialog(String title, Trailer trailer) {
		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);
		setResizable(false);

		this.trailer = trailer;

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	public TrailerDialog(String title) {
		this(title, null);
	}

	// -------------------------------------------------------------------------

	private TextField txfTrailerType, txfTrailerKapacitet;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblTrailerType = new Label("Trailer Type");
		pane.add(lblTrailerType, 0, 0);

		txfTrailerType = new TextField();
		pane.add(txfTrailerType, 0, 1);
		txfTrailerType.setPrefWidth(200);

		Label lblTrailerKapacitet = new Label("Trailer Kapicitet");
		pane.add(lblTrailerKapacitet, 0, 2);

		txfTrailerKapacitet = new TextField();
		pane.add(txfTrailerKapacitet, 0, 3);

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 5);
		GridPane.setHalignment(btnCancel, HPos.RIGHT);
		btnCancel.setOnAction(event -> cancelAction());

		Button btnCreate = new Button("Create Trailer");
		pane.add(btnCreate, 0, 5);
		GridPane.setHalignment(btnCreate, HPos.LEFT);
		btnCreate.setOnAction(event -> createAction());

		lblError = new Label();
		pane.add(lblError, 0, 11);
		lblError.setStyle("-fx-text-fill: red");

	}

	// -------------------------------------------------------------------------

	private void cancelAction() {
		// this.hide();
		close();
	}

	private void createAction() {

		String type = txfTrailerType.getText().trim();
		if (type.length() == 0) {
			lblError.setText("Udfyld trailer type");
			return;
		}
		String kapacitet = txfTrailerKapacitet.getText().trim();
		if (kapacitet.length() == 0) {
			lblError.setText("Udfyld kapacitet");
			return;
		} else {
			Trailer trailer = Service.createTrailer(
					Type.valueOf(txfTrailerType.getText()),
					Double.valueOf(txfTrailerKapacitet.getText()));

			hide();
		}
	}

}
