package guiFX;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import model.Lastbil;
import model.Status;
import storage.Storage;

public class StatistikPane extends GridPane {
	private Label antalOrdre;
	private Label antalLastbiler;
	private Label antalChauffører;
	private Label antalTrailere;
	private Label aktiveLastbiler;
	private int aktive;

	public StatistikPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		antalOrdre = new Label("Antal Ordre i systemmet:" + " "
				+ Storage.getAllOrdrer().size());
		this.add(antalOrdre, 1, 1);

		antalLastbiler = new Label("Antal Lastbiler i systemmet:" + " "
				+ Storage.getAllLastbiler().size());
		this.add(antalLastbiler, 1, 2);

		aktive = 0;
		for (Lastbil lastbil : Storage.getAllLastbiler()) {
			if (lastbil.getStatus() == Status.AKTIV) {
				aktive++;
			}
		}
		aktiveLastbiler = new Label("Antal Aktive Lastbiler: " + " " + aktive);
		this.add(aktiveLastbiler, 1, 3);

		antalChauffører = new Label("Antal Chauffører i systemmet:" + " "
				+ Storage.getAllChauffører().size());
		this.add(antalChauffører, 1, 4);

		antalTrailere = new Label("Antal Trailere i systemmet:" + " "
				+ Storage.getAllTrailerer().size());
		this.add(antalTrailere, 1, 5);

		Button refreshButton = new Button("Refresh");
		this.add(refreshButton, 1, 6);
		refreshButton.setOnAction(event -> this.refreshButton());
		refreshButton.setPrefSize(100, 50);
	}

	public void refreshButton() {
		antalOrdre.setText("Antal Ordre i systemmet:" + " "
				+ Storage.getAllOrdrer().size());
		antalLastbiler.setText("Antal Lastbiler i systemmet:" + " "
				+ Storage.getAllLastbiler().size());
		antalChauffører.setText("Antal Chauffører i systemmet:" + " "
				+ Storage.getAllChauffører().size());
		antalTrailere.setText("Antal Trailere i systemmet:" + " "
				+ Storage.getAllTrailerer().size());
		aktiveLastbiler.setText("Antal Aktive Lastbiler: " + " " + aktive);
	}

	// public void ankomstAction() {
	// AnkomstDialog dia = new AnkomstDialog();
	// dia.showAndWait();
	// }
}
