package guiFX;

import java.util.ArrayList;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Ankomst;
import service.Service;

public class InformationDialog extends Stage {

	public InformationDialog(String title) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private ArrayList<Ankomst> ankomster = new ArrayList<>();
	private Label lblLæsningStart;
	private Label lblLæsningSlut;
	private Label lblForventetAfgang;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		lblLæsningStart = new Label("Læsning Start:" + " "
				+ (Service.UdregnLæsningStart(AnkomstDialog.givAnkomst())));
		pane.add(lblLæsningStart, 0, 1);

		lblLæsningSlut = new Label("Læsning Slut: " + " "
				+ Service.UdregnLæsningSlut(AnkomstDialog.givAnkomst()));
		pane.add(lblLæsningSlut, 0, 2);

		lblForventetAfgang = new Label("Forventet Afgang: " + " "
				+ Service.UdregnTidligstAfgang(AnkomstDialog.givAnkomst()));
		pane.add(lblForventetAfgang, 0, 3);

		Button btnCancel = new Button("Ok");
		pane.add(btnCancel, 0, 12);
		GridPane.setHalignment(btnCancel, HPos.RIGHT);
		btnCancel.setOnAction(event -> this.cancelAction());
	}

	private void cancelAction() {
		this.hide();
		this.close();

	}

}
