package guiFX;

import java.util.ArrayList;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Ankomst;
import model.Rampe;
import model.Status;
import storage.Storage;

public class VægtStartDialog extends Stage {

	public VægtStartDialog(String title) {
		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);
		setResizable(false);

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	// -------------------------------------------------------------------------

	private ListView<Ankomst> lvwAnkomster = new ListView<>();
	private ArrayList<Ankomst> ankomster = new ArrayList<>();

	private Button btnRegistrer;

	private TextField txtstartvægt;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblAnkomstNavn = new Label("Vælg ankomst");
		pane.add(lblAnkomstNavn, 0, 0);

		pane.add(lvwAnkomster, 0, 1);
		lvwAnkomster.setPrefHeight(150);

		for (Rampe rampe : Storage.getAllRamper()) {
			for (Ankomst ankomst : rampe.getAnkomster()) {
				if (ankomst.getLastbil().getStatus() == Status.INAKTIV) {
					ankomster.add(ankomst);
				}
			}
		}

		lvwAnkomster.getItems().addAll(ankomster);

		Label lblstartvægt = new Label("Registreret vægt");
		pane.add(lblstartvægt, 0, 2);
		txtstartvægt = new TextField();
		txtstartvægt.setPrefWidth(20);
		pane.add(txtstartvægt, 0, 3);

		btnRegistrer = new Button("Registrer");
		pane.add(btnRegistrer, 0, 12);
		GridPane.setHalignment(btnRegistrer, HPos.LEFT);
		btnRegistrer.setOnAction(event -> registrerAction());

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 12);
		GridPane.setHalignment(btnCancel, HPos.RIGHT);
		btnCancel.setOnAction(event -> cancelAction());

		lblError = new Label();
		pane.add(lblError, 0, 11);
		lblError.setStyle("-fx-text-fill: red");

	}

	private void cancelAction() {
		hide();
		close();
	}

	private void registrerAction() {
		String vægt = txtstartvægt.getText().trim();
		if (vægt.length() == 0) {
			lblError.setText("Udfyld vægt");
			return;
		} else {
			lvwAnkomster.getSelectionModel().getSelectedItem()

			.setStartVægt(Double.parseDouble(txtstartvægt.getText()));
			lvwAnkomster.getSelectionModel().getSelectedItem().getLastbil()
			.setStatus(Status.AKTIV);
			txtstartvægt.clear();
			close();
		}

	}
}