package guiFX;

import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import model.Ankomst;
import model.Rampe;
import storage.Storage;

public class VisRampeOversigtPane extends GridPane {
	private ListView<Ankomst> lvwAnkomster;
	private ListView<Rampe> lvwRamper;
	private Label lbltelefon;
	private Label lbltidligstUd;
	private Label lblStart;
	private Label lblSlut;

	public VisRampeOversigtPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		Label rampeLabel = new Label("Ramper");
		this.add(rampeLabel, 1, 0);
		lvwRamper = new ListView<>();
		lvwRamper.getItems().setAll(Storage.getAllRamper());
		lvwRamper.setPrefSize(200, 200);
		this.add(lvwRamper, 1, 1);

		Label ankomstLabel = new Label("Ankomster");
		this.add(ankomstLabel, 2, 0);
		lvwAnkomster = new ListView<>();
		lvwAnkomster.setPrefSize(200, 200);
		this.add(lvwAnkomster, 2, 1);

		Button refreshButton = new Button("Refresh");
		this.add(refreshButton, 1, 2);
		GridPane.setHalignment(refreshButton, HPos.CENTER);
		refreshButton.setOnAction(event -> this.refreshButton());
		refreshButton.setPrefSize(100, 50);

		ChangeListener<Rampe> listenere = (ov, oldRampe, newRampe) -> this
				.selectedRampeChanged();
		lvwRamper.getSelectionModel().selectedItemProperty()
		.addListener(listenere);

		Button getTlfnummer = new Button("Vis Info");
		getTlfnummer.setPrefSize(125, 50);
		GridPane.setHalignment(getTlfnummer, HPos.CENTER);
		this.add(getTlfnummer, 2, 2);
		getTlfnummer.setOnAction(event -> this.getInfo());
		lbltelefon = new Label("Telefon Nummer:");
		this.add(lbltelefon, 1, 3);
		lbltidligstUd = new Label("Tidligst tid til afgang:");
		this.add(lbltidligstUd, 2, 3);
		lblStart = new Label("Tidligst start last");
		this.add(lblStart, 1, 4);
		lblSlut = new Label("Tidligst last slut");
		this.add(lblSlut, 2, 4);
	}

	private void selectedRampeChanged() {
		if (lvwRamper.getSelectionModel().getSelectedItem() != null) {
			lvwAnkomster.getItems().setAll(
					lvwRamper.getSelectionModel().getSelectedItem()
					.getAnkomster());

		}

	}

	private void refreshButton() {
		lvwRamper.getItems().setAll(Storage.getAllRamper());
		lvwAnkomster.getItems().clear();
		lbltelefon.setText("Telefon Nummer:");
		lbltidligstUd.setText("Tidligst tid til afgang:");
		lblStart.setText("Start på læssning:");
		lblSlut.setText("Slut på læssning:");
	}

	private void getInfo() {
		String tidlig = null;
		String start = null;
		String slut = null;
		if (lvwAnkomster.getSelectionModel().getSelectedItem() != null) {
			lbltelefon.setText("Telefon Nummer: "
					+ " "
					+ lvwAnkomster.getSelectionModel().getSelectedItem()
					.getLastbil().getChauffør().getTlfNummer());
		}
		if (lvwAnkomster.getSelectionModel().getSelectedItem() != null) {
			tidlig = lvwAnkomster.getSelectionModel().getSelectedItem()
					.getTidligstAfgang().toString();
			lbltidligstUd.setText("Tidligst tid til afgang: " + " " + tidlig);

		}

		if (lvwAnkomster.getSelectionModel().getSelectedItem() != null) {
			start = lvwAnkomster.getSelectionModel().getSelectedItem()
					.getLæsningStart().toString();
			lblStart.setText("Start på læssning:" + " " + start);
		}

		if (lvwAnkomster.getSelectionModel().getSelectedItem() != null) {
			slut = lvwAnkomster.getSelectionModel().getSelectedItem()
					.getLæsningSlut().toString();
			lblSlut.setText("Slut med læssning:" + " " + slut);
		}

	}
}
