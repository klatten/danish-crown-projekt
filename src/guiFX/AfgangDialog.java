package guiFX;

import java.util.ArrayList;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Ankomst;
import model.Rampe;
import model.Status;
import service.Service;
import storage.Storage;

public class AfgangDialog extends Stage {

	public AfgangDialog(String title) {
		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);
		setResizable(false);

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	// -------------------------------------------------------------------------

	private Label lblError;
	private static ListView<Ankomst> lvwAnkomster = new ListView<>();
	private ArrayList<Ankomst> ankomster = new ArrayList<>();
	public static TextField txtslutvægt;
	double slutvægt = 0.0;
	public static Ankomst ankomst;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblAnkomstNavn = new Label("Vælg ankomst");
		pane.add(lblAnkomstNavn, 0, 0);

		pane.add(AfgangDialog.lvwAnkomster, 0, 1);
		AfgangDialog.lvwAnkomster.setPrefHeight(150);

		for (Rampe rampe : Storage.getAllRamper()) {
			for (Ankomst ankomsts : rampe.getAnkomster()) {
				if (ankomsts.getLastbil().getStatus() == Status.AKTIV) {
					if (!ankomster.contains(ankomsts)) {
						ankomster.add(ankomsts);
					}
				}

			}
		}

		AfgangDialog.lvwAnkomster.getItems().setAll(ankomster);

		Label lblSlutVægt = new Label("Målt slutvægt");
		pane.add(lblSlutVægt, 0, 2);
		AfgangDialog.txtslutvægt = new TextField();
		pane.add(AfgangDialog.txtslutvægt, 0, 3);

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 12);
		GridPane.setHalignment(btnCancel, HPos.RIGHT);
		btnCancel.setOnAction(event -> cancelAction());

		Button btnMeldAfgang = new Button("Meld afgang");
		pane.add(btnMeldAfgang, 0, 12);
		GridPane.setHalignment(btnMeldAfgang, HPos.LEFT);
		btnMeldAfgang.setOnAction(event -> meldAfgang());

		lblError = new Label();
		pane.add(lblError, 0, 11);
		lblError.setStyle("-fx-text-fill: red");
	}

	private void cancelAction() {
		hide();
		close();

	}

	private void meldAfgang() {
		String vægt = AfgangDialog.txtslutvægt.getText().trim();
		if (vægt.length() == 0) {
			lblError.setText("Udfyld vægt");
			return;
		} else {
			AfgangDialog.lvwAnkomster
			.getSelectionModel()
			.getSelectedItem()
			.setSlutVægt(
					Double.parseDouble(AfgangDialog.txtslutvægt
							.getText()));
			if (Service.UdregnVægtAccept(AfgangDialog.lvwAnkomster
					.getSelectionModel().getSelectedItem()) == false) {
				Service.foranIKøen(AfgangDialog.lvwAnkomster
						.getSelectionModel().getSelectedItem());
				NegativVægtDialog();
			} else {
				AfgangDialog.lvwAnkomster.getSelectionModel().getSelectedItem()
				.getLastbil().setStatus(Status.INAKTIV);
				Ankomst ankomste = AfgangDialog.lvwAnkomster
						.getSelectionModel().getSelectedItem();
				Service.afgangAfAnkomst(ankomste);
				PositivVægtDialog();
				close();
			}
		}

	}

	public static Ankomst givAnkomst() {
		AfgangDialog.ankomst = AfgangDialog.lvwAnkomster.getSelectionModel()
				.getSelectedItem();
		return AfgangDialog.ankomst;
	}

	public static Double givText() {
		return Double.parseDouble(AfgangDialog.txtslutvægt.getText());

	}

	public void PositivVægtDialog() {
		PositivVægtDialog dia = new PositivVægtDialog("Succes!");
		dia.showAndWait();
	}

	public void NegativVægtDialog() {
		NegativVægtDialog dia = new NegativVægtDialog("Failure!");
		dia.showAndWait();
	}

}