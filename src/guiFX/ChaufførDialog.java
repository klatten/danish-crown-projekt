package guiFX;

import java.time.LocalTime;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Chauffør;
import service.Service;

public class ChaufførDialog extends Stage {

	private Chauffør chauffør;

	public ChaufførDialog(String title, Chauffør chauffør) {
		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);
		setResizable(false);

		this.chauffør = chauffør;

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	public ChaufførDialog(String title) {
		this(title, null);
	}

	// -------------------------------------------------------------------------

	private TextField txfChaufførNavn, txfTlf, txfTidligstTid;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblChaufførNavn = new Label("Navn");
		pane.add(lblChaufførNavn, 0, 0);

		txfChaufførNavn = new TextField();
		pane.add(txfChaufførNavn, 0, 1);
		txfChaufførNavn.setPrefWidth(200);

		Label lblTlf = new Label("TelefonNr");
		pane.add(lblTlf, 0, 2);

		txfTlf = new TextField();
		pane.add(txfTlf, 0, 3);

		Label lblTidligstTid = new Label("Tidligst Afgang");
		pane.add(lblTidligstTid, 0, 4);

		txfTidligstTid = new TextField();
		pane.add(txfTidligstTid, 0, 5);

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 10);
		GridPane.setHalignment(btnCancel, HPos.RIGHT);
		btnCancel.setOnAction(event -> cancelAction());

		Button btnCreate = new Button("Create Chauffør");
		pane.add(btnCreate, 0, 10);
		GridPane.setHalignment(btnCreate, HPos.LEFT);
		btnCreate.setOnAction(event -> createAction());

		lblError = new Label();
		pane.add(lblError, 0, 11);
		lblError.setStyle("-fx-text-fill: red");

		// this.initControls();
	}

	// private void initControls()
	// {
	// if (firma != null) {
	// txfChaufførNavn.setText(firma.getNavn());
	// txfTidligstTid.setText("" + firma.getFirmaTlf());
	// txftlf.setText("" + firma.getCvr());
	//
	// }
	// else {
	// txfChaufførNavn.clear();
	// txfTidligstTid.clear();
	// txftlf.clear();
	//
	// }
	// }

	// -------------------------------------------------------------------------

	private void cancelAction() {
		// this.hide();
		close();
	}

	private void createAction() {
		String name = txfChaufførNavn.getText().trim();
		if (name.length() == 0) {
			lblError.setText("Udfyld navn");
			return;
		}
		String tlf = txfTlf.getText().trim();
		if (tlf.length() == 0) {
			lblError.setText("Udfyld telefonnummer");
			return;
		}
		String tid = txfTidligstTid.getText().trim();
		if (tid.length() == 0) {
			lblError.setText("Udfyld Dato");
			return;
		} else {
			Chauffør chauffør = Service.createChauffør(
					txfChaufførNavn.getText(), txfTlf.getText(),
					LocalTime.parse(txfTidligstTid.getText()));

			hide();
		}
	}
}
