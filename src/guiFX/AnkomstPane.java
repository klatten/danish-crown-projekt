package guiFX;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class AnkomstPane extends GridPane {
	public AnkomstPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		Button meldAnkomst = new Button("Meld Ankomst");
		this.add(meldAnkomst, 1, 1);
		meldAnkomst.setOnAction(event -> this.ankomstAction());

		Button meldAfgang = new Button("Meld Afgang");
		this.add(meldAfgang, 1, 3);
		meldAnkomst.setPrefSize(150, 50);
		meldAfgang.setPrefSize(150, 50);
		meldAfgang.setOnAction(event -> this.afgangAction());

		Button registrerStartvægt = new Button("Registrer startvægt");
		this.add(registrerStartvægt, 1, 2);
		registrerStartvægt.setPrefSize(150, 50);
		registrerStartvægt.setOnAction(event -> this.registrerVægtAction());

	}

	public void ankomstAction() {
		AnkomstDialog dia = new AnkomstDialog("Meld Ankomst");
		dia.showAndWait();

	}

	public void registrerVægtAction() {
		VægtStartDialog dia = new VægtStartDialog("Registrer Vægt");
		dia.showAndWait();
	}

	public void afgangAction() {
		AfgangDialog dia = new AfgangDialog("Meld Afgang");
		dia.showAndWait();
	}
}
