package guiFX;

import java.time.LocalTime;
import java.util.ArrayList;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Ankomst;
import model.Rampe;
import model.Status;
import storage.Storage;

public class TidPane extends GridPane {
	private ListView<Ankomst> lvwAnkomster;
	private LocalTime nutid;
	private Label nutidlbl;
	private TextField setTidtxt;
	private Button time;
	private Button minut;
	private ButtonBase tredvemin;

	public TidPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		setTidtxt = new TextField("Tidspunkt som eks 10:30");
		this.add(setTidtxt, 0, 0);
		nutidlbl = new Label("Sat tid");
		this.add(nutidlbl, 0, 3);
		GridPane.setHalignment(nutidlbl, HPos.CENTER);

		Button setTid = new Button("Set tid");
		this.add(setTid, 1, 0);
		setTid.setOnAction(event -> this.setTid());
		setTid.setPrefSize(100, 150);

		tredvemin = new Button("Tilføj 30 min");
		this.add(tredvemin, 0, 5);
		tredvemin.setOnAction(event -> this.tredveButton());
		time = new Button("Tilføj 1 time");
		tredvemin.setPrefSize(100, 150);

		this.add(time, 0, 6);
		GridPane.setHalignment(time, HPos.CENTER);
		GridPane.setHalignment(tredvemin, HPos.CENTER);
		time.setOnAction(event -> this.timeButton());
		time.setPrefSize(100, 150);

		minut = new Button("Tilføj 1 minut");
		this.add(minut, 0, 4);
		GridPane.setHalignment(minut, HPos.CENTER);
		minut.setOnAction(event -> this.minutButton());
		minut.setPrefSize(100, 150);

		if (nutid == null) {
			tredvemin.setVisible(false);
			minut.setVisible(false);
			time.setVisible(false);
			nutidlbl.setVisible(false);
		}
		Label besked = new Label("Chauffører klar til afgang:");
		this.add(besked, 0, 1);
		lvwAnkomster = new ListView<>();
		this.add(lvwAnkomster, 0, 2);

	}

	public void setTid() {
		nutid = LocalTime.parse(setTidtxt.getText());
		nutidlbl.setText(this.getNutid().toString());
		tredvemin.setVisible(true);
		minut.setVisible(true);
		time.setVisible(true);
		nutidlbl.setVisible(true);
	}

	private LocalTime getNutid() {
		return nutid;
	}

	public void tredveButton() {
		nutid = nutid.plusMinutes(30);
		this.refresh();
	}

	public void timeButton() {
		nutid = nutid.plusHours(1);
		this.refresh();
	}

	public void minutButton() {
		nutid = nutid.plusMinutes(1);
		this.refresh();
	}

	public void refresh() {
		nutidlbl.setText(this.getNutid().toString());
		this.setAnkomster();
	}

	public void setAnkomster() {
		ArrayList<Ankomst> ankomster = new ArrayList<>();
		for (Rampe rampe : Storage.getAllRamper()) {
			for (Ankomst ankomst : rampe.getAnkomster()) {
				if (ankomst.getLastbil().getStatus() == Status.AKTIV) {
					if (ankomst.getTidligstAfgang().isBefore(nutid)) {
						ankomster.add(ankomst);
					}
				}
			}
		}
		lvwAnkomster.getItems().setAll(ankomster);
	}

}
