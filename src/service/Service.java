package service;

import java.time.LocalDate;
import java.time.LocalTime;

import model.Ankomst;
import model.Chauffør;
import model.Delordre;
import model.Lastbil;
import model.Læssemedarbejder;
import model.Ordre;
import model.Rampe;
import model.Status;
import model.Trailer;
import model.Type;
import storage.Storage;

public class Service {

	private static int i = 0;

	/**
	 * @param lastbil
	 * @return returnere den tid hvor en rampe af samme type som den givne
	 *         Ankomst tidligst kan komme til. Den givne ankomsts læsningStart
	 *         bliver sat som den returnerede tid.
	 */
	public static LocalTime UdregnLæsningStart(Ankomst ankomst) {
		LocalTime tidligTid = LocalTime.now();
		Rampe rampes = Storage.getAllRamper().get(0);
		for (Rampe rampe : Storage.getAllRamper()) {
			if (rampe.getType() == ankomst.getLastbil().getTrailer().getType()) {
				if (rampe.getTidligstTid().isBefore(rampes.getTidligstTid())) {
					tidligTid = rampe.getTidligstTid();
					rampes = rampe;

				}

			}
		}
		ankomst.setLæsningStart(tidligTid);

		return tidligTid;
	}

	/**
	 * @param lastbil
	 * @return returnere det tidspunkt som den givne Ankomst forventes læsset.
	 *         Den givne Ankomsts forventede sluttid sættes til det returnerede
	 *         tidspunkt
	 */
	public static LocalTime UdregnLæsningSlut(Ankomst ankomst) {
		int tid = 0;
		LocalTime tidligTid = LocalTime.now();
		for (Delordre delordrer : ankomst.getLastbil().getDelordrer()) {
			tid += delordrer.getLæssetid();
		}
		tidligTid = Service.UdregnLæsningStart(ankomst).plusMinutes(tid);

		ankomst.setLæsningSlut(tidligTid);

		return tidligTid;
	}

	/**
	 * @param lastbil
	 * @return returnere det tidligste tidspunkt hvor en lastbil kan forlade
	 *         Danish Crown Checker chauførrens tidligsafgang og læssekøen for
	 *         hans lasbil, og finder den der sker sidst
	 */
	public static LocalTime UdregnTidligstAfgang(Ankomst ankomst) {
		if (ankomst.getLastbil().getChauffør().getTidligstAfgang()
				.isBefore(Service.UdregnLæsningSlut(ankomst))) {
			ankomst.setTidligstAfgang(Service.UdregnLæsningSlut(ankomst));

			return Service.UdregnLæsningSlut(ankomst);
		} else {
			ankomst.setTidligstAfgang(ankomst.getLastbil().getChauffør()
					.getTidligstAfgang());
			return ankomst.getLastbil().getChauffør().getTidligstAfgang();
		}

	}

	/**
	 * @param lastbil
	 * @return Returnere en boolean der angiver om en lastbils vægt er inden for
	 *         den accepterede vægt grænse. hvis false returneres bør
	 *         foranIKøen() anvendes
	 */
	public static boolean UdregnVægtAccept(Ankomst ankomst) {
		Ordre ordrer = new Ordre(Type.PALLE, 0, 0, 0);
		double vægt = 0.0;
		for (Delordre delordre : ankomst.getLastbil().getDelordrer()) {
			ordrer = delordre.getOrdre();
			vægt += delordre.getVægt();
		}
		if ((ankomst.getStartVægt() + vægt)
				* (1 + (ordrer.getMargenVedVejning() / 100)) >= ankomst
					.getSlutVægt()
				&& (ankomst.getStartVægt() + vægt)
				* (1 - (ordrer.getMargenVedVejning() / 100)) <= ankomst
							.getSlutVægt())
			return true;
		else
			return false;
	}

	/**
	 * @param ankomst
	 *            Sætter en lastbil forest i køen på den rampe der snarest er
	 *            færdig med dens nuværende læsning
	 */
	public static void foranIKøen(Ankomst ankomst) {
		LocalTime tidligTid = LocalTime.now();
		Rampe rampes = Storage.getAllRamper().get(0);
		for (Rampe rampe : Storage.getAllRamper()) {
			if (rampe.getType() == ankomst.getLastbil().getTrailer().getType()) {
				if (rampe.getTidligstTid().isBefore(rampes.getTidligstTid())) {
					tidligTid = rampe.getTidligstTid();
					rampes = rampe;
				}
			}
		}
		rampes.getAnkomster().add(0, ankomst);
	}

	/**
	 * @param ankomst
	 *            sætter den givne ankomst bagerst i den korteste kø på en rampe
	 *            med samme type som ankomstens trailer. Hvis der ikke er nogen
	 *            kø på nogle ramper sættes den på den rampe med lavest rampe
	 *            nummer
	 */
	public static void acceptRampeValg(Ankomst ankomst) {
		Rampe rampes = Storage.getAllRamper().get(i);
		for (Rampe rampe : Storage.getAllRamper()) {
			if (rampe.getType() == ankomst.getLastbil().getTrailer().getType()) {
				if (rampe.getTidligstTid().isBefore(rampes.getTidligstTid())) {
					rampes = rampe;
				}
			}
		}
		if (rampes.getType() == ankomst.getLastbil().getTrailer().getType()) {
			rampes.addAnkomst(ankomst);
		} else {
			i++;
			Service.acceptRampeValg(ankomst);
		}

	}

	/**
	 * @param ankomst
	 *            Fjerner den givne ankomst fra en rampes kø.
	 */
	public static void afgangAfAnkomst(Ankomst ankomst) {
		Ankomst anko = ankomst;
		Rampe rampes = Storage.getAllRamper().get(0);
		for (Rampe rampe : Storage.getAllRamper()) {
			for (Ankomst ankomste : rampe.getAnkomster()) {
				if (ankomste == ankomst) {
					anko = ankomst;
					rampes = rampe;
				}
			}
		}
		rampes.getAnkomster().remove(anko);
	}

	/**
	 * @param navn
	 * @param tlfnummer
	 * @return returnere et nyt Chauffør objekt med de givne parametre. Gemmer
	 *         det returnerede objekt i storage.
	 */
	public static Chauffør createChauffør(String navn, String tlfnummer,
			LocalTime tidligstTiladtAfgang) {
		Chauffør nyChauffør = new Chauffør(navn, tlfnummer,
				tidligstTiladtAfgang);
		Storage.gemChauffør(nyChauffør);
		return nyChauffør;
	}

	/**
	 * @param type
	 * @param kapacitet
	 * @return returnere et nyt Trailer objekt med de givne parametre. Gemmer
	 *         det returnerede objekt i storage.
	 */
	public static Trailer createTrailer(Type type, double kapacitet) {
		Trailer nyTrailer = new Trailer(type, kapacitet);
		Storage.gemTrailer(nyTrailer);
		return nyTrailer;
	}

	/**
	 * @param nr
	 * @param chauffør
	 * @param trailer
	 * @param delOrdre
	 * @return returnere et nyt Lastbil objekt med de givne parametre. Gemmer
	 *         det returnerede objekt i storage.
	 */
	public static Lastbil createLastbil(int nr, Chauffør chauffør,
			Trailer trailer) {
		Lastbil nyLastbil = new Lastbil(nr, chauffør, trailer);
		Storage.gemLastbil(nyLastbil);
		return nyLastbil;

	}

	/**
	 * @param type
	 * @param bruttovægt
	 * @param margenVedVejning
	 * @param nr
	 * @return returnere et nyt Ordre objekt med de givne parametre. Gemmer det
	 *         returnerede objekt i storage.
	 */
	public static Ordre createOrdre(Type type, double bruttovægt,
			int margenVedVejning, int nr) {
		Ordre nyOrdre = new Ordre(type, bruttovægt, margenVedVejning, nr);
		Storage.gemOrdre(nyOrdre);
		return nyOrdre;
	}

	/**
	 * @param ordre
	 * @param vægt
	 * @param læssetid
	 * @param læsseDato
	 * @return returnere et nyt Delordre objekt med de givne parametre. Gemmer
	 *         delordren på den givne ordre
	 */
	public static Delordre createDelordre(Ordre ordre, double vægt,
			int læssetid, LocalDate læsseDato) {
		Delordre nyDelordre = new Delordre(ordre, vægt, læssetid, læsseDato);
		ordre.addDelordre(nyDelordre);
		return nyDelordre;

	}

	/**
	 * @param tidspunkt
	 * @param lastbil
	 * @return returnere et nyt Ankomst objekt med de givne parametre. Gemmer
	 *         det returnerede objekt i det givne Lastbil objekt.
	 */
	public static Ankomst createAnkomst(LocalTime tidspunkt, Lastbil lastbil) {
		Ankomst nyAnkomst = new Ankomst(tidspunkt, lastbil);
		lastbil.addAnkomst(nyAnkomst);
		return nyAnkomst;
	}

	/**
	 * Laver nogle objekter til brug i Test og i guiFx
	 */

	public static void createSomeObjects() {

		// Laver Chaufføre
		Chauffør chauffør1 = Service.createChauffør("Dave Hammerfield",
				"11235813", LocalTime.of(23, 15));
		Chauffør chauffør2 = Service.createChauffør("Niels Hammerfield",
				"01123581", LocalTime.of(23, 15));
		Chauffør chauffør3 = Service.createChauffør("Justing Time", "01189998",
				LocalTime.of(23, 30));
		Chauffør chauffør4 = Service.createChauffør("Line Finestead",
				"81999119", LocalTime.of(13, 50));
		Chauffør chauffør5 = Service.createChauffør("Poul Svenson", "72539110",
				LocalTime.of(12, 00));
		Chauffør chauffør6 = Service.createChauffør("Sigmund Lunding",
				"77971114", LocalTime.of(14, 00));
		Chauffør chauffør7 = Service.createChauffør("Michael Fugt", "15823585",
				LocalTime.of(12, 30));
		Chauffør chauffør8 = Service.createChauffør("Susanne Klausen",
				"98524564", LocalTime.of(13, 20));

		// Laver Traillere
		// Todo muligvis nødvendigt at rette på kapacitet senere

		Trailer ktrailer1 = Service.createTrailer(Type.KAR, 250); // ordre1
		// delo1
		Trailer ktrailer2 = Service.createTrailer(Type.KAR, 200); // ordre1
		// delo2
		Trailer ktrailer3 = Service.createTrailer(Type.KAR, 500); // ordre2
		// delo1
		Trailer jtrailer1 = Service.createTrailer(Type.JULETRÆ, 550); // ordre3
		// delo1
		Trailer jtrailer2 = Service.createTrailer(Type.JULETRÆ, 300); // ordre3
		// delo2
		// &
		// ordre4 delo1
		Trailer jtrailer3 = Service.createTrailer(Type.JULETRÆ, 500); // ordre4
		// delo2
		Trailer ptrailer1 = Service.createTrailer(Type.PALLE, 400); // ordre5
		// delo1
		Trailer ptrailer2 = Service.createTrailer(Type.PALLE, 300); // ordre6
		// delo1

		// Laver Ramper
		Rampe kar1 = new Rampe(Type.KAR, 1);
		Storage.gemRamper(kar1);
		Rampe kar2 = new Rampe(Type.KAR, 2);
		Storage.gemRamper(kar2);
		Rampe kar3 = new Rampe(Type.KAR, 3);
		Storage.gemRamper(kar3);
		Rampe palle1 = new Rampe(Type.PALLE, 4);
		Storage.gemRamper(palle1);
		Rampe palle2 = new Rampe(Type.PALLE, 5);
		Storage.gemRamper(palle2);
		Rampe palle3 = new Rampe(Type.PALLE, 6);
		Storage.gemRamper(palle3);
		Rampe juletræ1 = new Rampe(Type.JULETRÆ, 7);
		Storage.gemRamper(juletræ1);
		Rampe juletræ2 = new Rampe(Type.JULETRÆ, 8);
		Storage.gemRamper(juletræ2);
		Rampe juletræ3 = new Rampe(Type.JULETRÆ, 9);
		Storage.gemRamper(juletræ3);

		// Laver Lastbiler
		Lastbil lastbil1 = Service.createLastbil(1, chauffør1, ktrailer1);
		Lastbil lastbil2 = Service.createLastbil(2, chauffør2, ktrailer2);
		Lastbil lastbil3 = Service.createLastbil(3, chauffør3, ktrailer3);
		Lastbil lastbil4 = Service.createLastbil(4, chauffør4, jtrailer1);
		Lastbil lastbil5 = Service.createLastbil(5, chauffør5, jtrailer2);
		Lastbil lastbil6 = Service.createLastbil(6, chauffør6, jtrailer3);
		Lastbil lastbil7 = Service.createLastbil(7, chauffør7, ptrailer1);
		Lastbil lastbil8 = Service.createLastbil(8, chauffør8, ptrailer2);

		// Laver Læssemedarbejdre
		Læssemedarbejder arbejder1 = new Læssemedarbejder("Henning Hansen");
		Læssemedarbejder arbejder2 = new Læssemedarbejder("Stig Lund");

		Storage.getAllRamper().get(0).addLæssemedarbejder(arbejder1);
		Storage.getAllRamper().get(0).addLæssemedarbejder(arbejder2);

		// Laver Ordre // opdeling i delordre
		Service.createOrdre(Type.KAR, 300, 10, 1); // 100, 200
		Service.createOrdre(Type.KAR, 500.00, 10, 2); // 500
		Service.createOrdre(Type.JULETRÆ, 550, 5, 3); // 500, 50
		Service.createOrdre(Type.JULETRÆ, 800, 7, 4); // 300, 500
		Service.createOrdre(Type.PALLE, 400, 10, 5); // 400
		Service.createOrdre(Type.PALLE, 300, 10, 6); // 300

		// Laver Delordre
		// todo
		Delordre delordre1 = Service.createDelordre(
				Storage.getAllOrdrer().get(0), 200.00, 15,
				LocalDate.of(2015, 4, 27));
		Storage.getAllLastbiler().get(0).addDelordre(delordre1);

		Delordre delordre2 = Service.createDelordre(
				Storage.getAllOrdrer().get(0), 100.00, 10,
				LocalDate.of(2015, 4, 27));
		Storage.getAllLastbiler().get(1).addDelordre(delordre2);

		Delordre delordre3 = Service.createDelordre(
				Storage.getAllOrdrer().get(1), 500.00, 30,
				LocalDate.of(2015, 4, 27));
		Storage.getAllLastbiler().get(2).addDelordre(delordre3);

		Delordre delordre4 = Service.createDelordre(
				Storage.getAllOrdrer().get(2), 500.00, 30,
				LocalDate.of(2015, 4, 27));
		Storage.getAllLastbiler().get(3).addDelordre(delordre4);

		Delordre delordre5 = Service.createDelordre(
				Storage.getAllOrdrer().get(3), 50.00, 10,
				LocalDate.of(2015, 4, 27));
		Storage.getAllLastbiler().get(4).addDelordre(delordre5);

		Delordre delordre6 = Service.createDelordre(
				Storage.getAllOrdrer().get(3), 300.00, 25,
				LocalDate.of(2015, 4, 27));
		Storage.getAllLastbiler().get(7).addDelordre(delordre6);

		Delordre delordre7 = Service.createDelordre(
				Storage.getAllOrdrer().get(4), 500.00, 30,
				LocalDate.of(2015, 4, 27));
		Storage.getAllLastbiler().get(5).addDelordre(delordre7);

		Delordre delordre8 = Service.createDelordre(
				Storage.getAllOrdrer().get(4), 400.00, 35,
				LocalDate.of(2015, 4, 27));
		Storage.getAllLastbiler().get(6).addDelordre(delordre8);

		Delordre delordre9 = Service.createDelordre(
				Storage.getAllOrdrer().get(5), 100.00, 15,
				LocalDate.of(2015, 4, 27));
		Storage.getAllLastbiler().get(7).addDelordre(delordre9);

		// Opretter Ankomster
		Ankomst karAnkomst1 = Service.createAnkomst(LocalTime.now(), Storage
				.getAllLastbiler().get(0));
		karAnkomst1.getLastbil().setStatus(Status.AKTIV);
		Service.UdregnLæsningStart(karAnkomst1);
		Service.UdregnLæsningSlut(karAnkomst1);
		Service.UdregnTidligstAfgang(karAnkomst1);
		Ankomst karAnkomst2 = Service.createAnkomst(LocalTime.now(), Storage
				.getAllLastbiler().get(1));
		Service.acceptRampeValg(karAnkomst2);
		karAnkomst2.getLastbil().setStatus(Status.AKTIV);
		Service.UdregnLæsningStart(karAnkomst2);
		Service.UdregnLæsningSlut(karAnkomst2);
		Service.UdregnTidligstAfgang(karAnkomst2);
		Ankomst karAnkomst3 = Service.createAnkomst(LocalTime.now(), Storage
				.getAllLastbiler().get(2));
		Service.acceptRampeValg(karAnkomst1);
		Service.acceptRampeValg(karAnkomst3);
		karAnkomst3.getLastbil().setStatus(Status.AKTIV);
		Service.UdregnLæsningStart(karAnkomst3);
		Service.UdregnLæsningSlut(karAnkomst3);
		Service.UdregnTidligstAfgang(karAnkomst3);
		Ankomst julAnkomst1 = Service.createAnkomst(LocalTime.now(), Storage
				.getAllLastbiler().get(3));
		Service.acceptRampeValg(julAnkomst1);
		julAnkomst1.getLastbil().setStatus(Status.AKTIV);
		Service.UdregnLæsningStart(julAnkomst1);
		Service.UdregnLæsningSlut(julAnkomst1);
		Service.UdregnTidligstAfgang(julAnkomst1);
		Ankomst julAnkomst2 = Service.createAnkomst(LocalTime.now(), Storage
				.getAllLastbiler().get(4));
		Service.acceptRampeValg(julAnkomst2);
		julAnkomst2.getLastbil().setStatus(Status.AKTIV);
		Service.UdregnLæsningStart(julAnkomst2);
		Service.UdregnLæsningSlut(julAnkomst2);
		Service.UdregnTidligstAfgang(julAnkomst2);
		Ankomst julAnkomst3 = Service.createAnkomst(LocalTime.now(), Storage
				.getAllLastbiler().get(5));
		Service.acceptRampeValg(julAnkomst3);
		julAnkomst3.getLastbil().setStatus(Status.AKTIV);
		Service.UdregnLæsningStart(julAnkomst3);
		Service.UdregnLæsningSlut(julAnkomst3);
		Service.UdregnTidligstAfgang(julAnkomst3);
		Ankomst palAnkomst1 = Service.createAnkomst(LocalTime.now(), Storage
				.getAllLastbiler().get(6));
		palAnkomst1.getLastbil().setStatus(Status.AKTIV);
		Service.UdregnLæsningStart(palAnkomst1);
		Service.UdregnLæsningSlut(palAnkomst1);
		Service.UdregnTidligstAfgang(palAnkomst1);
		Service.acceptRampeValg(palAnkomst1);
		Ankomst palAnkomst2 = Service.createAnkomst(LocalTime.now(), Storage
				.getAllLastbiler().get(7));
		Service.acceptRampeValg(palAnkomst2);
		palAnkomst2.getLastbil().setStatus(Status.AKTIV);
		Service.UdregnLæsningStart(palAnkomst2);
		Service.UdregnLæsningSlut(palAnkomst2);
		Service.UdregnTidligstAfgang(palAnkomst2);

	}

}
