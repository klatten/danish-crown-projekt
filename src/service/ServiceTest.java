package service;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import model.Ankomst;
import model.Chauffør;
import model.Delordre;
import model.Lastbil;
import model.Ordre;
import model.Status;
import model.Trailer;
import model.Type;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import storage.Storage;

public class ServiceTest {

	private static Ankomst ankomst;


	@BeforeClass
	public static void setUp(){
		Service.createSomeObjects();
		ankomst = Storage.getAllLastbiler().get(0).getAnkomster().get(0);
	}
	

	
	//udregner tidligstLæsningsStart ud fra køen på rampen, da der ikke er nogen kø starer den med det samme.
	@Test
	public void testUdregnLæsningStart() throws Exception {
		assertEquals(LocalTime.now(),Service.UdregnLæsningStart(ankomst));

	}
	//udregner udregnLæsningsSlut for "Ankomst". Foventet return er 15 minutter efter start.
	@Test	
	public void testUdregnLæsningSlut() throws Exception{
		assertEquals(Service.UdregnLæsningStart(ankomst).plusMinutes(15),Service.UdregnLæsningSlut(ankomst));
	}
	
	//udregner udregnTidligstAfgang for "ankomst", Chaufføren for "Ankomst" kan tidligst køre 23.15
	@Test 
	public void testUdregnTidligstAfgang() throws Exception{
		Ankomst ankomst = Storage.getAllLastbiler().get(0).getAnkomster().get(0);
		assertEquals(LocalTime.of(23,15),Service.UdregnTidligstAfgang(ankomst));
	}
	
	//Laver en ny ankomst med det der skal til, og tilføjer den til den tidligste rampe. den tidligste rampe har en kø på 15 minutter.
	//Så 15 minutter lægges til de 20 fra delordren læssetid.
	@Test
	public void testacceptRampevalg() throws Exception{
			Chauffør testDriver = Service.createChauffør("Steve Test", "30594683", LocalTime.of(23,15));
			Trailer testTrailer = Service.createTrailer(Type.KAR, 500);
			Lastbil testLastbil = Service.createLastbil(100, testDriver, testTrailer);
			Ordre testOrdre =  Service.createOrdre(Type.KAR, 500, 10, 100);
			Delordre testDel1 = Service.createDelordre(testOrdre, 500, 20, LocalDate.of(2015, 4, 12));
			testOrdre.addDelordre(testDel1);
			testLastbil.addDelordre(testDel1);
			Ankomst testAnkomst = Service.createAnkomst(LocalTime.of(23, 15), testLastbil);
			testLastbil.addAnkomst(testAnkomst);
			Service.createAnkomst(LocalTime.of(23, 15), testLastbil);
			Service.acceptRampeValg(testAnkomst);
			testAnkomst.getLastbil().setStatus(Status.AKTIV);
			assertEquals(LocalTime.now().plusMinutes(20+15),Service.UdregnLæsningSlut(testLastbil.getAnkomster().get(0)));
			Service.afgangAfAnkomst(testAnkomst);
	}	


	
	//laver 1 ny ankomster og det der høre til, sætter den på den første ledige rampe. Rampe 1 har nu 2 objector i sit
	//ankomst Array. Jeg fjerner herefter det første og checker at den nye ankomst er først i køen.
	@Test
	public void testafgangAnkomst() throws Exception{
		
		Chauffør testDriver = Service.createChauffør("Steve Test", "30594683", LocalTime.of(23,15));
		Trailer testTrailer = Service.createTrailer(Type.KAR, 500);
		Lastbil testLastbil = Service.createLastbil(100, testDriver, testTrailer);
		Ordre testOrdre =  Service.createOrdre(Type.KAR, 500, 10, 100);
		Delordre testDel1 = Service.createDelordre(testOrdre, 500, 15, LocalDate.of(2015, 4, 12));
		testLastbil.addDelordre(testDel1);
		Ankomst testAnkomst = Service.createAnkomst(LocalTime.of(23, 15), testLastbil);
		Service.createAnkomst(LocalTime.of(23, 15), testLastbil);
		Service.acceptRampeValg(testAnkomst);
		testAnkomst.getLastbil().setStatus(Status.AKTIV);
		
		Service.afgangAfAnkomst(Storage.getAllRamper().get(0).getAnkomster().get(0));
		assertEquals(LocalTime.now().plusMinutes(0),Service.UdregnLæsningStart(testLastbil.getAnkomster().get(0)));
	}
		@Test
		public void testUdregnVægtAccept() throws Exception{
		ankomst.setSlutVægt(ankomst.getStartVægt()+ankomst.getLastbil().getDelordrer().get(0).getVægt());
		assertEquals(true,Service.UdregnVægtAccept(ankomst));
		}
		
		//Laver 1 ny Ankomst og bruger metoden foran i køen på den, der bør derefter være først på den rampe
		//der tidligst er ledig hvilket er rampe 1.
		@Test 
		public void testForanIKøen(){
			Chauffør testDriver2 = Service.createChauffør("Eric Vincent III", "25845685", LocalTime.of(14,30));
			Trailer testTrailer2 = Service.createTrailer (Type.KAR, 250);
			Lastbil testLastbil2 = Service.createLastbil(500, testDriver2, testTrailer2);
			Ordre testOrdre2 = Service.createOrdre(Type.KAR, 250, 5, 250);
			Delordre testDelordre21 = Service.createDelordre(testOrdre2, 250, 15, LocalDate.of(2015, 04, 25));
			testLastbil2.addDelordre(testDelordre21);
			Ankomst testAnkomst2 = Service.createAnkomst(LocalTime.of(14, 30), testLastbil2);
			Service.foranIKøen(testAnkomst2);
			assertEquals(testAnkomst2,Storage.getAllRamper().get(0).getAnkomster().get(0));
			Service.afgangAfAnkomst(testAnkomst2);
			
			
		}
		
		
		
}