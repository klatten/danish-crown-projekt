package model;

import java.time.LocalTime;

public class Ankomst {

	// Fields
	private LocalTime tidspunkt;
	private double StartVægt;
	private double SlutVægt;
	private LocalTime tidligstAfgang;
	private LocalTime læsningStart;
	private LocalTime læsningSlut;
	private Lastbil lastbil;

	// Constructor

	public Ankomst(LocalTime tidspunkt, Lastbil lastbil) {
		this.tidspunkt = tidspunkt;
		this.lastbil = lastbil;
	}

	// Getters & setters
	public LocalTime getTidspunkt() {
		return tidspunkt;
	}

	public Lastbil getLastbil() {
		return lastbil;
	}

	public void setLastbil(Lastbil lastbil) {
		this.lastbil = lastbil;
	}

	public void setTidspunkt(LocalTime tidspunkt) {
		this.tidspunkt = tidspunkt;
	}

	public double getStartVægt() {
		return StartVægt;
	}

	public void setStartVægt(double StartVægt) {
		this.StartVægt = StartVægt;
	}

	public double getSlutVægt() {
		return SlutVægt;
	}

	public void setSlutVægt(double SlutVægt) {
		this.SlutVægt = SlutVægt;
	}

	public LocalTime getTidligstAfgang() {
		return tidligstAfgang;
	}

	public void setTidligstAfgang(LocalTime tidligstAfgang) {
		this.tidligstAfgang = tidligstAfgang;
	}

	public LocalTime getLæsningStart() {
		return læsningStart;
	}

	public void setLæsningStart(LocalTime læsningStart) {
		this.læsningStart = læsningStart;
	}

	public LocalTime getLæsningSlut() {
		return læsningSlut;
	}

	public void setLæsningSlut(LocalTime læsningSlut) {
		this.læsningSlut = læsningSlut;
	}

	@Override
	public String toString() {
		return "Ankomst tid: " + " " + tidspunkt.toString() + " "
				+ lastbil.getChauffør().getNavn() + " " + "Afgangstid:" + " "
				+ this.getTidligstAfgang();
	}
}
