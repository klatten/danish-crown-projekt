package model;

import java.util.ArrayList;

public class Lastbil {

	// Fields
	private int nr;
	private ArrayList<Delordre> delordrer = new ArrayList<Delordre>();
	private ArrayList<Ankomst> ankomster = new ArrayList<Ankomst>();
	private Chauffør chauffør;
	private Trailer trailer;
	private Status status;

	// Constructor
	public Lastbil(int nr, Chauffør chauffør, Trailer trailer) {

		this.nr = nr;
		this.chauffør = chauffør;
		this.trailer = trailer;
	}

	// Getters & setters
	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	public Chauffør getChauffør() {
		return chauffør;
	}

	public void setChauffør(Chauffør chauffør) {
		this.chauffør = chauffør;
	}

	public Trailer getTrailer() {
		return trailer;
	}

	public void setTrailer(Trailer trailer) {
		this.trailer = trailer;
	}

	public ArrayList<Delordre> getDelordrer() {
		return delordrer;
	}

	public ArrayList<Ankomst> getAnkomster() {
		return ankomster;
	}

	public void setAnkomster(ArrayList<Ankomst> ankomster) {
		this.ankomster = ankomster;
	}

	// ArrayList add/remove

	public void addAnkomst(Ankomst ankomst) {
		ankomster.add(ankomst);
	}

	public void removeAnkomst(Ankomst ankomst) {
		ankomster.remove(ankomst);
	}

	public void addDelordre(Delordre delordre) {
		delordrer.add(delordre);
	}

	public void removeDelordre(Delordre delordre) {
		delordrer.remove(delordre);
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Status getStatus() {
		return this.status;
	}

	@Override
	public String toString() {
		return Integer.toString(nr);
	}
}
