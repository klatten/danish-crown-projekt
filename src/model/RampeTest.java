package model;

import static org.junit.Assert.*;

import java.time.LocalTime;

import org.junit.BeforeClass;
import org.junit.Test;

import service.Service;
import storage.Storage;

public class RampeTest {


	private static Rampe rampe;
	private static Ankomst ankomst;

	@BeforeClass
	public static void setUp() throws Exception {
		Service.createSomeObjects();
		rampe = Storage.getAllRamper().get(0);
		ankomst = rampe.getAnkomster().get(0);
	}

	@Test
	public void testGetType() {
		assertEquals(Type.KAR,rampe.getType());
	}

	@Test public void testSetType(){
		rampe.setType(Type.PALLE);
		assertEquals(Type.PALLE,rampe.getType());
		rampe.setType(Type.KAR);
	}
	
	@Test
	public void testGetNummer(){
		assertEquals(1,rampe.getNummer(),0);
	}
	
	@Test
	public void testSetNummer(){
		rampe.setNummer(10);
		assertEquals(10,rampe.getNummer(),0);
		rampe.setNummer(1);
	}
	
	//Da lastbil nr2 er den første til at køre metoden acceptRampevalg i createSomeObjects er det den lastbil som er på rampe 1.
	//Da det er sværere at sammenligne 2 hele arrays har vi valgt at sammenligne en smule information man kan hente ved hjælp af Arrayet i stedet for.
	@Test 
	public void testGetAnkomster(){
		assertEquals(2,rampe.getAnkomster().get(0).getLastbil().getNr());
	}
	
	@Test
	public void testRemoveAnkomst(){
		rampe.removeAnkomst(ankomst);
		assertEquals(0,rampe.getAnkomster().size(),0);
		rampe.addAnkomst(ankomst);
	}
	
	@Test 
		public void testAddAnkomst(){
		rampe.removeAnkomst(ankomst);
		int ankomstMængde = rampe.getAnkomster().size(); 
		rampe.addAnkomst(ankomst);
		assertEquals(1, ankomstMængde + 1);
	}
	
	@Test
	public void testGetLæssemedarbejdere(){
		assertEquals(2,rampe.getLæssemedarbejdere().size(),0);
	}
	
	@Test
	public void testRemoveLæssemedarbejder(){
		rampe.removeLæssemedarbejder(rampe.getLæssemedarbejdere().get(0));
		assertEquals(1,rampe.getLæssemedarbejdere().size(),0);
		rampe.addLæssemedarbejder(rampe.getLæssemedarbejdere().get(0));
	}
	
	@Test
	public void testAddLæssemedarbejder(){
		Læssemedarbejder test = new Læssemedarbejder("Bob");
		rampe.addLæssemedarbejder(test);
		assertEquals(3,rampe.getLæssemedarbejdere().size(),0);
		rampe.removeLæssemedarbejder(test);
	}
	
	
	//Ankomsterne på rampe 1 har en sammenlagt lææsse tid på 10 minutter, så rampen skulle være ledig 10 minutter
	//fra metoder bliver kørt.
	@Test
	public void testGetTidligstTid(){
		assertEquals(LocalTime.now().plusMinutes(10),rampe.getTidligstTid());
	}
	
	@Test
	public void testToString(){
		assertEquals("Rampe nr: 1 KAR",rampe.toString());
	}
}
