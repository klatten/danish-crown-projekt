package model;

import java.time.LocalDate;

public class Delordre {

	// Fields
	private double vægt;
	private int læssetid;
	private LocalDate læssedato;
	private Ordre ordre;

	// Constructor
	public Delordre(Ordre ordre, double vægt, int læssetid, LocalDate læssedato) {
		this.vægt = vægt;
		this.læssetid = læssetid;
		this.læssedato = læssedato;
		this.ordre = ordre;
	}

	// Getters & setters
	public Ordre getOrdre() {
		return ordre;
	}

	public void setOrdre(Ordre ordre) {
		this.ordre = ordre;
	}

	public double getVægt() {
		return vægt;
	}

	public void setVægt(double vægt) {
		this.vægt = vægt;
	}

	public int getLæssetid() {
		return læssetid;
	}

	public void setLæssetid(int læssetid) {
		this.læssetid = læssetid;
	}

	public LocalDate getLæssedato() {
		return læssedato;
	}

	public void setLæssedato(LocalDate læssedato) {
		this.læssedato = læssedato;
	}

	@Override
	public String toString() {
		return getOrdre().getNr() + "." + getOrdre().getDelordrer().size()
				+ " " + Double.toString(vægt) + "kg" + " "
				+ Integer.toString(læssetid) + " " + "Minutter";
	}

}
