package model;

public class Læssemedarbejder {
	
	
	//Fields
	
	private String navn;

	//Constructor
	public Læssemedarbejder(String navn) {
		this.navn = navn;
	}
	//Getters & setters
	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}
	
	

}
