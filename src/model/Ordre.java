package model;

import java.util.ArrayList;

public class Ordre {

	// Fields
	private Type type;
	private double bruttovægt;
	private int margenVedVejning;
	private int nr;
	private ArrayList<Delordre> delordrer = new ArrayList<Delordre>();

	// Constructor
	public Ordre(Type type, double bruttovægt, int margenVedVejning, int nr) {
		this.type = type;
		this.bruttovægt = bruttovægt;
		this.margenVedVejning = margenVedVejning;
		this.nr = nr;
	}

	// Getters & setters
	public double getBruttovægt() {
		return bruttovægt;
	}

	public void setBruttovægt(double bruttovægt) {
		this.bruttovægt = bruttovægt;
	}

	public int getMargenVedVejning() {
		return margenVedVejning;
	}

	public void setMargenVedVejning(int margenVedVejning) {
		this.margenVedVejning = margenVedVejning;
	}

	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public ArrayList<Delordre> getDelordrer() {
		return delordrer;
	}

	// AraryList add/remove

	public void addDelordre(Delordre delordre) {
		delordrer.add(delordre);
	}

	public void removeDelordre(Delordre delordre) {
		delordrer.remove(delordre);
	}

	@Override
	public String toString() {
		return "Ordrenr" + " " + Integer.toString(nr) + " " + type.toString()
				+ " " + "Fejlmargen: " + " "
				+ Integer.toString(margenVedVejning);
	}

}
