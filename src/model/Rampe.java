package model;

import java.time.LocalTime;
import java.util.ArrayList;

public class Rampe {

	// Fields
	private Type type;
	private int nummer;
	private ArrayList<Ankomst> ankomster = new ArrayList<Ankomst>();
	private ArrayList<Læssemedarbejder> læssemedarbejdere = new ArrayList<Læssemedarbejder>();

	// Constructor

	public Rampe(Type type, int nummer) {
		this.nummer = nummer;
		this.type = type;

	}

	// Getters & setters

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public ArrayList<Ankomst> getAnkomster() {
		return ankomster;
	}

	// ArrayList add/remove

	public void removeAnkomst(Ankomst ankomst) {
		ankomster.remove(ankomst);
	}

	public void addAnkomst(Ankomst ankomst) {
		ankomster.add(ankomst);
	}

	public ArrayList<Læssemedarbejder> getLæssemedarbejdere() {
		return læssemedarbejdere;
	}

	public void addLæssemedarbejder(Læssemedarbejder læssemedarbejder) {
		læssemedarbejdere.add(læssemedarbejder);
	}

	public void removeLæssemedarbejder(Læssemedarbejder læssemedarbejder) {
		læssemedarbejdere.remove(læssemedarbejder);
	}

	/**
	 * @return den tid som rampen er ledig.
	 */
	
	public LocalTime getTidligstTid() {
		int tid = 0;
		for (Ankomst ankomste : ankomster) {
			for (Delordre delordre : ankomste.getLastbil().getDelordrer()) {
				tid += delordre.getLæssetid();

			}
		}
		return LocalTime.now().plusMinutes(tid);
	}

	@Override
	public String toString() {
		return "Rampe nr: " + Integer.toString(nummer) + " "
				+ type.toString();
	}
}
