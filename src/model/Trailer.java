package model;

public class Trailer {

	// Fields
	private Type type;
	private double kapacitet;

	// Constructor
	public Trailer(Type type, double kapacitet) {
		this.type = type;
		this.kapacitet = kapacitet;
	}

	// Getters & setters
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public double getKapacitet() {
		return kapacitet;
	}

	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}

	@Override
	public String toString() {
		return Double.toString(kapacitet) + "m3" + " " + type.toString();
	}

}
