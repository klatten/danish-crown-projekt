package model;

import java.time.LocalTime;

public class Chauffør {

	// Fields
	private String navn;
	private String tlfNummer;
	private LocalTime tidligstAfgang;

	// Constructor
	public Chauffør(String navn, String tlfNummer, LocalTime tidligsAfgang) {
		this.navn = navn;
		this.tlfNummer = tlfNummer;
		this.tidligstAfgang = tidligsAfgang;
	}

	// Getters & setters
	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getTlfNummer() {
		return tlfNummer;
	}

	public void setTlfNummer(String tlfNummer) {
		this.tlfNummer = tlfNummer;
	}

	public LocalTime getTidligstAfgang() {
		return tidligstAfgang;
	}

	public void setTidligsAfgang(LocalTime tidligstAfgang) {
		this.tidligstAfgang = tidligstAfgang;
	}

	@Override
	public String toString() {
		return navn.toString();
	}

}
